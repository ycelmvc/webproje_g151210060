﻿using System.Web;
using System.Web.Mvc;

namespace webproje_g151210060
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
