﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Bu kod araç tarafından oluşturuldu.
//     Çalışma Zamanı Sürümü:4.0.30319.42000
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod yeniden oluşturulursa kaybolur.
// </auto-generated>
//------------------------------------------------------------------------------

namespace webproje_g151210060.Resources {
    using System;
    
    
    /// <summary>
    ///   Yerelleştirilmiş dizeleri aramak gibi işlemler için, türü kesin olarak belirtilmiş kaynak sınıfı.
    /// </summary>
    // Bu sınıf ResGen veya Visual Studio gibi bir araç kullanılarak StronglyTypedResourceBuilder
    // sınıfı tarafından otomatik olarak oluşturuldu.
    // Üye eklemek veya kaldırmak için .ResX dosyanızı düzenleyin ve sonra da ResGen
    // komutunu /str seçeneğiyle yeniden çalıştırın veya VS projenizi yeniden oluşturun.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Bu sınıf tarafından kullanılan, önbelleğe alınmış ResourceManager örneğini döndürür.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("webproje_g151210060.Resources.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Tümü için geçerli iş parçacığının CurrentUICulture özelliğini geçersiz kular
        ///   CurrentUICulture özelliğini tüm kaynak aramaları için geçersiz kılar.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Abone Ol benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string AboneOl {
            get {
                return ResourceManager.GetString("AboneOl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Anasayfa benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Anasayfa {
            get {
                return ResourceManager.GetString("Anasayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Arama benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Arama {
            get {
                return ResourceManager.GetString("Arama", resourceCulture);
            }
        }
        
        /// <summary>
        ///   destek benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string destek {
            get {
                return ResourceManager.GetString("destek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   En iyi Filmler benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string EniyiFilmler {
            get {
                return ResourceManager.GetString("EniyiFilmler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Film Bilgi Sistemi benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string FilmBilgiSistemi {
            get {
                return ResourceManager.GetString("FilmBilgiSistemi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Güncel Filmler benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string GüncelFilm {
            get {
                return ResourceManager.GetString("GüncelFilm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Hakkımızda benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Hakkimizda {
            get {
                return ResourceManager.GetString("Hakkimizda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Hedefler benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Hedefler {
            get {
                return ResourceManager.GetString("Hedefler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Hepsi benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Hepsi {
            get {
                return ResourceManager.GetString("Hepsi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   İletişim benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string İletisim {
            get {
                return ResourceManager.GetString("İletisim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Iletişim Bilgileri benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string IletisimBilgileri {
            get {
                return ResourceManager.GetString("IletisimBilgileri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Kategoriler benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Kategoriler {
            get {
                return ResourceManager.GetString("Kategoriler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Telif Hakkı benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string Telifhakkı {
            get {
                return ResourceManager.GetString("Telifhakkı", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Yeni Filmler benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string YeniFilmler {
            get {
                return ResourceManager.GetString("YeniFilmler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   En popüler Filmler benzeri yerelleştirilmiş bir dize arar.
        /// </summary>
        public static string YüksekKaliteFilm {
            get {
                return ResourceManager.GetString("YüksekKaliteFilm", resourceCulture);
            }
        }
    }
}
